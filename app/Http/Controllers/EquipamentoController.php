<?php

namespace App\Http\Controllers;

use App\Models\Equipamento;
use Illuminate\Http\Request;

class EquipamentoController extends Controller
{
    public function index()
    {
        $equipamento = Equipamento::all();

        return response()->json([
            'success' => true,
            'data' => $equipamento
        ]);
    }

    public function show($id)
    {
        $equipamento = Equipamento::find($id);

        if (!$equipamento) {
            return response()->json([
                'success' => false,
                'message' => 'Equipamento no encontrado'
            ], 400);
        }

        return response()->json([
            'success' => true,
            'data' => $equipamento
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'equipamento' => 'required'
        ]);

        $equipamento = Equipamento::create($request->all());

        if ($equipamento)
            return response()->json([
                'success' => true,
                'data' => $equipamento
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Equipamento no fue registrado'
            ], 500);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'equipamento' => 'required'
        ]);

        $equipamento = Equipamento::find($id);

        if (!$equipamento) {
            return response()->json([
                'success' => false,
                'message' => 'Equipamento no existe'
            ], 400);
        }

        $updated = $equipamento->fill($request->all())->save();

        if ($updated)
            return response()->json([
                'success' => true
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Equipamento no actualizo'
            ], 500);
    }

    public function destroy($id)
    {
        $equipamento = Equipamento::find($id);;

        if (!$equipamento) {
            return response()->json([
                'success' => false,
                'message' => 'Equipamento no se pudo encontrar'
            ], 400);
        }

        if ($equipamento->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Equipamento no se puedo eliminar'
            ], 500);
        }
    }
}
