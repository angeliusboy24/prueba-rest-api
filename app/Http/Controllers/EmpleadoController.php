<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Empleado;
use Illuminate\Support\Facades\DB;

class EmpleadoController extends Controller
{
    public function index()
    {
        $empleado = DB::table('empleado')
            ->join('cargo', 'empleado.cargo_idcargo', '=', 'cargo.idcargo')
            ->get();

        return response()->json([
            'success' => true,
            'data' => $empleado
        ]);
    }

    public function show($id)
    {
        $empleado = Empleado::find($id);

        if (!$empleado) {
            return response()->json([
                'success' => false,
                'message' => 'Empleado no encontrado'
            ], 400);
        }

        return response()->json([
            'success' => true,
            'data' => $empleado
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nombres' => 'required',
            'ap_paterno' => 'required',
            'ap_materno' => 'required',
            'cargo_idcargo' => 'required',
            'complejo_idcomplejo' => 'required'
        ]);

        $empleado = Empleado::create($request->all());

        if ($empleado)
            return response()->json([
                'success' => true,
                'data' => $empleado
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Empleado no fue registrado'
            ], 500);
    }

    public function update(Request $request, $id)
    {
        $empleado = Empleado::find($id);

        if (!$empleado) {
            return response()->json([
                'success' => false,
                'message' => 'Empleado no existe'
            ], 400);
        }

        $updated = $empleado->fill($request->all())->save();

        if ($updated)
            return response()->json([
                'success' => true
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Empleado no ha sido actualizado'
            ], 500);
    }

    public function destroy($id)
    {
        $empleado = Empleado::find($id);;

        if (!$empleado) {
            return response()->json([
                'success' => false,
                'message' => 'Empleado no existe'
            ], 400);
        }

        if ($empleado->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Empleado eliminado'
            ], 500);
        }
    }
}
