<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Complejo;

class ComplejoController extends Controller
{
    public function index()
    {
        $complejo = Complejo::all();

        return response()->json([
            'success' => true,
            'data' => $complejo
        ]);
    }

    public function show($id)
    {
        $complejo = Complejo::find($id);

        if (!$complejo) {
            return response()->json([
                'success' => false,
                'message' => 'Post not found '
            ], 400);
        }

        return response()->json([
            'success' => true,
            'data' => $complejo->toArray()
        ], 400);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'cargo' => 'required'
        ]);

        $complejo = new Complejo();
        $complejo->sede_idsede = $request->sede_idsede;
        $complejo->tipo_complejo_idtipo_complejo = $request->tipo_complejo_idtipo_complejo;
        $complejo->localizacion = $request->localizacion;

        if (Complejo::create($complejo))
            return response()->json([
                'success' => true,
                'data' => $complejo->toArray()
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Post not added'
            ], 500);
    }

    public function update(Request $request, $id)
    {
        $complejo = Complejo::find($id);

        if (!$complejo) {
            return response()->json([
                'success' => false,
                'message' => 'Post not found'
            ], 400);
        }

        $updated = $complejo->fill($request->all())->save();

        if ($updated)
            return response()->json([
                'success' => true
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Post can not be updated'
            ], 500);
    }

    public function destroy($id)
    {
        $complejo = Complejo::find($id);;

        if (!$complejo) {
            return response()->json([
                'success' => false,
                'message' => 'Post not found'
            ], 400);
        }

        if ($complejo->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post can not be deleted'
            ], 500);
        }
    }
}
