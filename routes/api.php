<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'App\Http\Controllers\PassportAuthController@register');
Route::post('/login', 'App\Http\Controllers\PassportAuthController@login');

Route::middleware('auth:api')->group(function () {
    Route::resource('empleado', 'App\Http\Controllers\EmpleadoController');
    Route::resource('cargo', 'App\Http\Controllers\CargoController');
    Route::resource('complejo', 'App\Http\Controllers\ComplejoController');
    Route::resource('equipamento', 'App\Http\Controllers\EquipamentoController');
});
